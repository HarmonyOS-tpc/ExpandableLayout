# ExpandableLayout
  ExpandableLayout: An expandable layout container for openharmony.
# ExpandableLayout includes:
An openharmony layout class that supports animating the expansion and collapse of its child views.
This library offloads as much work as possible to the parent layout to ensure that the rendering and
size calculations behave as expected. This means that even complex features such as DirectionLayout's weight tag are properly supported.

# Usage Instructions
 1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the demo/ folder.

 2. The following core classes and xml are the essential interface to create the expandablelayout:
    expandable layout :- 
	 <net.cachapa.expandablelayout.ExpandableLayout
        ohos:id="$+id:expandable_layout_1"
        ohos:width="match_parent"
        ohos:height="0"
        ohos:weight="1"
        ohos:background_element="#f00"
        app:el_duration="1000"
        app:el_expanded="true"
        app:el_parallax="0.5">

        <Text
            ohos:text_size="15fp"
            ohos:text_alignment="center"
            ohos:width="match_parent"
            ohos:height="match_parent"
            ohos:background_element="#00f"
            ohos:foreground_alignment="center"
            ohos:text="Fill height"
            ohos:text_color="#fff" />

    </net.cachapa.expandablelayout.ExpandableLayout>
	
	and java function
	
	  private ExpandableLayout expandableLayout0;
	  expandableLayout0.setExpanded(true, false);
        expandableLayout0.collapse(false);
        expandableLayout0.setDuration(1000);
        
# Installation Instructions

    1.For using expandable lib module in sample app,include the below library dependency to generate hap file:
    Add the dependencies in entry/build.gradle as below :

    dependencies {
    implementation project(path ':library’)
    }

    2. Using the library har file , make sure to add the library.har file in the entry/libs folder and add the below
    dependency in build.gradle.

    Modify the dependencies in the entry/build.gradle file.
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    }
	3. For using ExpandableLayout from a remote repository in separate application, add the below dependencies and include "library.har" in libs folder of "entry" module  :

    	Modify entry build.gradle as below :
    	```
    	dependencies {
    	    implementation fileTree(dir: 'libs', include: ['*.har'])
    	    implementation 'io.openharmony.tpc.thirdlib:ExpandableLayout:1.0.1'
		}
        ```
 # License
  Copyright 2016 Daniel Cachapa.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 


