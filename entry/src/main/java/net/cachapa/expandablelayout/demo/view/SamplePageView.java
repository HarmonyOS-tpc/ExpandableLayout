/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * SamplePageView.
 */
public class SamplePageView extends AbstractPageView {
    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public SamplePageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public SamplePageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public SamplePageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_simple,
                        null, false);
        if (layout.findComponentById(ResourceTable.Id_expandable_layout_0) instanceof ExpandableLayout) {
            expandableLayout0 = (ExpandableLayout) layout.findComponentById(ResourceTable.Id_expandable_layout_0);
        }
        if (layout.findComponentById(ResourceTable.Id_expandable_layout_1) instanceof ExpandableLayout) {
            expandableLayout1 = (ExpandableLayout) layout.findComponentById(ResourceTable.Id_expandable_layout_1);
        }
        if (layout.findComponentById(ResourceTable.Id_expand_button_text) instanceof Text) {
            Text expandableText = (Text) layout.findComponentById(ResourceTable.Id_expand_button_text);
            initValue(expandableText);
        }
        return layout;
    }

    private void initValue(Text expandableText) {
        expandableLayout0.setOnExpansionUpdateListener((expansionFraction, state) ->
                LogUtil.debug("ExpandableLayout", "simpleState0: " + state));

        expandableLayout1.setOnExpansionUpdateListener((expansionFraction, state) ->
                LogUtil.debug("ExpandableLayout", "simpleState1: " + state));
        expandableLayout0.setExpanded(true, true);
        expandableLayout1.setExpanded(true, true);

        expandableText.setClickedListener(component -> {
            LogUtil.debug("Expandable", "" + expandableLayout0.isExpanded());
            if (expandableLayout0.isExpanded()) {
                LogUtil.debug("Expandable", "if" + expandableLayout0.isExpanded());
                expandableLayout0.collapse();
            } else if (expandableLayout1.isExpanded()) {
                LogUtil.debug("Expandable", "else if" + expandableLayout1.isExpanded());
                expandableLayout1.collapse();
            } else {
                LogUtil.debug("Expandable", "else0" + expandableLayout0.isExpanded());
                LogUtil.debug("Expandable", "else1" + expandableLayout1.isExpanded());
                expandableLayout0.expand();
                expandableLayout1.expand();
            }
        });
    }
}
