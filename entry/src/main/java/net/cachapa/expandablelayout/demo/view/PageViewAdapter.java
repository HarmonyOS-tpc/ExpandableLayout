/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.demo.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSliderProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * PageViewAdapter.
 */
public class PageViewAdapter extends PageSliderProvider {
    private static final String TAG = PageViewAdapter.class.getCanonicalName();
    private AbilitySlice slice;
    private List<? extends PageInfo> pageViews;

    /**
     * PageViewAdapter.
     *
     * @param abilitySlice AbilitySlice
     * @param list         Pagelist
     */
    public PageViewAdapter(AbilitySlice abilitySlice, List<? extends PageInfo> list) {
        slice = abilitySlice;
        if (list != null) {
            pageViews = list;
            return;
        }
        pageViews = new ArrayList();
    }

    /**
     * isPageMatchToObject.
     *
     * @param component component
     * @param obj Object
     * @return true
     */
    public boolean isPageMatchToObject(Component component, Object obj) {
        return true;
    }

    @Override
    public String getPageTitle(int position) {
        List<AbstractPageView> abstractPageViews = (List<AbstractPageView>) pageViews;
        return abstractPageViews.get(position).getName();
    }

    /**
     * getCount.
     *
     * @return integer
     */
    public int getCount() {
        return this.pageViews.size();
    }

    /**
     * createPageInContainer.
     *
     * @param componentContainer ComponentContainer
     * @param ival integer
     * @return object Object
     */
    public Object createPageInContainer(ComponentContainer componentContainer, int ival) {
        Component directionalLayout = new DirectionalLayout(slice);
        if (ival >= 0 && ival < this.pageViews.size()) {
            directionalLayout = pageViews.get(ival).getRootView();
        }
        componentContainer.addComponent(directionalLayout);
        return directionalLayout;
    }

    /**
     * destroyPageFromContainer.
     *
     * @param componentContainer ComponentContainer
     * @param idle nteger
     * @param obj Object
     */

    public void destroyPageFromContainer(ComponentContainer componentContainer, int idle, Object obj) {
        if (componentContainer == null) {
            LogUtil.debug(TAG, "destory item failed, container is null");
        } else if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        }
    }
}
