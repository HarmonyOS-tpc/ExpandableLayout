/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.Const;
import net.cachapa.expandablelayout.demo.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

/**
 * ListPageView.
 */
public class ListPageView extends AbstractPageView {
    private ListContainer list;
    private AbilitySlice abilitySlice;
    private SimpleAdapter simpleAdapter;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public ListPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
        this.abilitySlice = abilitySlice;
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public ListPageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public ListPageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_list_container,
                        null, false);
        if (layout.findComponentById(ResourceTable.Id_recycler_view) instanceof ListContainer) {
            list = (ListContainer) layout.findComponentById(ResourceTable.Id_recycler_view);
        }
        initValue();
        return layout;
    }

    private void initValue() {
        simpleAdapter = new SimpleAdapter(list);
        list.setItemProvider(simpleAdapter);
        simpleAdapter.notifyDataChanged();
    }

    /**
     * SimpleAdapter
     */
    public class SimpleAdapter extends BaseItemProvider implements ExpandableLayout.OnExpansionUpdateListener {
        private static final int UNSELECTED = -1;
        private ListPageView listPageView;
        private ListContainer recyclerView;
        private int selectedItem = UNSELECTED;


        /**
         * SimpleAdapter constructor
         *
         * @param recyclerView list
         */
        public SimpleAdapter(ListContainer recyclerView) {
            this.listPageView = listPageView;
            this.recyclerView = recyclerView;
        }

        @Override
        public int getCount() {
            return Const.TWELVE;
        }

        @Override
        public Object getItem(int postion) {
            return postion;
        }

        @Override
        public long getItemId(int val) {
            return val;
        }


        @Override
        public Component getComponent(int position, Component rootView, ComponentContainer componentContainer) {
            Component component = rootView;
            ComponentHolder holder;
            if (component == null) {
                component = LayoutScatter.getInstance(abilitySlice).parse(ResourceTable.Layout_sample_list,
                        componentContainer, false);
            }
            holder = new ComponentHolder();
            if (component.findComponentById(ResourceTable.Id_expandable_layout_list) instanceof ExpandableLayout) {
                holder.expandableLayout = (ExpandableLayout)
                        component.findComponentById(ResourceTable.Id_expandable_layout_list);
            }
            holder.expandableLayout.setOnExpansionUpdateListener(this);
            if (component.findComponentById(ResourceTable.Id_expand_button_list) instanceof Text) {
                holder.expandButton = (Text) component.findComponentById(ResourceTable.Id_expand_button_list);
            }
            int value = (int) getItemId(position);
            boolean isSelected = position == selectedItem;

            holder.expandButton.setText(value + ". Tap to expand");
            holder.expandButton.setSelected(isSelected);
            holder.expandableLayout.setExpanded(isSelected, false);
            holder.expandableLayout.setExpanded(true, false);
            holder.expandableLayout.collapse(false);
            holder.expandableLayout.setDuration(1000);
            component.setTag(holder);
            if (component.getTag() instanceof ComponentHolder) {
                holder = (ComponentHolder) component.getTag();
            }
            bind(holder, value);
            return component;
        }

        private void bind(ComponentHolder holder, int pos) {
            holder.expandButton.setClickedListener(component -> {
                simpleAdapter.notifyDataChanged();
                if (selectedItem != -1) {
                    holder.expandButton.setSelected(false);
                    holder.expandableLayout.collapse();
                    notifyDataChanged();
                    LogUtil.debug("selectedId", "selectedItem holder1=" + selectedItem);
                }
                int position = recyclerView.getItemPosByVisibleIndex(pos);
                if (position == selectedItem) {
                    selectedItem = UNSELECTED;
                    LogUtil.debug("selectedId", "selectedItem=" + position);
                } else {
                    holder.expandButton.setSelected(true);
                    holder.expandableLayout.setExpanded(true, true);
                    holder.expandableLayout.collapse(true);
                    holder.expandableLayout.expand(true);
                    selectedItem = position;
                    LogUtil.debug("selectedId", "selectedItem else=" + selectedItem);
                }
            });
        }

        class ComponentHolder {
            private ExpandableLayout expandableLayout;
            private Text expandButton;
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            LogUtil.debug("ExpandableLayout", "State: " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                recyclerView.setCentralScrollMode(true);
            }
        }
    }
}

