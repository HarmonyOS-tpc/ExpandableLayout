/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.slice;

import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.Const;
import net.cachapa.expandablelayout.demo.util.LogUtil;
import net.cachapa.expandablelayout.demo.util.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

/**
 * MainAbilitySlice.
 */
public class MainAbilitySlice extends AbilitySlice {
    private PositionLayout mRootId;
    private ListContainer listComponent;
    private String[] list = {"ViewPager"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
            ComponentContainer rootLayout = (ComponentContainer)
                    LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_main,
                            null, false);
        if (rootLayout.findComponentById(ResourceTable.Id_parentid) instanceof PositionLayout) {
            mRootId = (PositionLayout) rootLayout.findComponentById(ResourceTable.Id_parentid);
        }
        mRootId.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
        if (mRootId.findComponentById(ResourceTable.Id_lc_main) instanceof ListContainer) {
            listComponent = (ListContainer) mRootId.findComponentById(ResourceTable.Id_lc_main);
        }
        populateListContainer();
        super.setUIContent(rootLayout);
    }

    @Override
    protected void onForeground(Intent intent) {
        LogUtil.info("MainScreen", "onForeground");
        super.onForeground(intent);
        populateListContainer();
    }

    /**
     * populateListContainer
     */
    private void populateListContainer() {
        listComponent.setItemProvider(new MainListProvider(list));
        listComponent.setItemClickedListener((listContainer, component, value, val) -> {
            Intent intent1 = new Intent();
            intent1.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
            switch (value) {
                case Const.ZERO:
                    present(new SampleViewPagerSlice(), intent1);
                    break;
                default:
            }
        });
    }

    /**
     * MainListProvider.
     */
    public class MainListProvider extends BaseItemProvider {
        String[] listItems;

        MainListProvider(String[] lst) {
            listItems = lst;
        }

        @Override
        public int getCount() {
            return listItems.length;
        }

        @Override
        public Object getItem(int val) {
            return listItems[val];
        }

        @Override
        public long getItemId(int value) {
            return Const.ZERO;
        }

        @Override
        public Component getComponent(int value, Component component, ComponentContainer componentContainer) {
            Component components = component;
            if (components == null) {
                components = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_list_item,
                        componentContainer, false);
            }
            if (components.findComponentById(ResourceTable.Id_list_component) instanceof Button) {
                ((Button) (components.findComponentById(ResourceTable.Id_list_component))).setText((String)
                        getItem(value));
            }
            components.setClickable(false);
            return components;
        }
    }
}