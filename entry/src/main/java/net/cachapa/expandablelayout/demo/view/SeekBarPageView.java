/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;

/**
 * SeekBarPageView.
 */
public class SeekBarPageView extends AbstractPageView implements
        Component.ClickedListener, Slider.ValueChangedListener, ExpandableLayout.OnExpansionUpdateListener {
    private ExpandableLayout expandableLayout;
    private Slider seekbar;
    private Component content;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public SeekBarPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public SeekBarPageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public SeekBarPageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_seek_bar,
                        null, false);
        if (layout.findComponentById(ResourceTable.Id_seek_bar) instanceof Slider) {
            seekbar = (Slider) layout.findComponentById(ResourceTable.Id_seek_bar);
        }
        content = layout.findComponentById(ResourceTable.Id_content);
        if (layout.findComponentById(ResourceTable.Id_expandable_seek_layout) instanceof ExpandableLayout) {
            expandableLayout = (ExpandableLayout) layout.findComponentById(ResourceTable.Id_expandable_seek_layout);
        }
        initValue();
        return layout;
    }

    private void initValue() {
        seekbar.setValueChangedListener(this);
        expandableLayout.setOnExpansionUpdateListener(this);
    }

    @Override
    public void onExpansionUpdate(float expansionFraction, int state) {
        content.setScaleX(expansionFraction);
        content.setScaleY(expansionFraction);
        content.setAlpha(expansionFraction);
    }

    @Override
    public void onClick(Component component) {
    }

    @Override
    public void onProgressUpdated(Slider slider, int val, boolean bab) {
        expandableLayout.setExpansion(slider.getProgress() / (float) slider.getMax());
    }

    @Override
    public void onTouchStart(Slider slider) {
    }

    @Override
    public void onTouchEnd(Slider slider) {
    }
}
