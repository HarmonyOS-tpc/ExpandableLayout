/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.slice;

import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.Const;
import net.cachapa.expandablelayout.demo.util.ResUtil;
import net.cachapa.expandablelayout.demo.view.*;
import net.cachapa.expandablelayout.demo.view.indicator.PageSliderIndicatorExt;
import net.cachapa.expandablelayout.demo.view.tab.TabInfo;
import net.cachapa.expandablelayout.demo.view.tab.TabListListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * SampleViewPagerSlice.
 */
public class SampleViewPagerSlice extends AbilitySlice {
    private DirectionalLayout mDlViewRoot;
    private PageSlider mPager;
    private TabList mTabIndicator;
    private PageSliderIndicatorExt mColorIndicator;
    private PageViewAdapter pageViewAdapter;

    private List<AbstractPageView> mPageViews;
    private String[] pageText = {"Simple", "Accordion", "List", "Horizontal", "Seekbar"};

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer layout = null;

        if (LayoutScatter.getInstance(this).
                parse(ResourceTable.Layout_slice_slider_tabs, null, false)
                instanceof ComponentContainer) {
            layout = (ComponentContainer) LayoutScatter.getInstance(this).
                    parse(ResourceTable.Layout_slice_slider_tabs, null, false);
        }
        if (layout.findComponentById(ResourceTable.Id_dlRootLayout) instanceof DirectionalLayout) {
            mDlViewRoot = (DirectionalLayout) layout.findComponentById(ResourceTable.Id_dlRootLayout);
        }
        mDlViewRoot.setBackground(ResUtil.buildDrawableByColor((Color.DKGRAY).getValue()));
        if (layout.findComponentById(ResourceTable.Id_slider) instanceof PageSlider) {
            mPager = (PageSlider) layout.findComponentById(ResourceTable.Id_slider);
        }
        if (layout.findComponentById(ResourceTable.Id_tlTabLayout) instanceof TabList) {
            mTabIndicator = (TabList) layout.findComponentById(ResourceTable.Id_tlTabLayout);
        }
        mColorIndicator = (PageSliderIndicatorExt) layout.findComponentById(ResourceTable.Id_indicator);
        initPageView(intent);
        initTabs();
        setUIContent(layout);
    }

    /**
     * initPageView.
     *
     * @param intent Intent
     */
    private void initPageView(Intent intent) {
        initPager(Color.WHITE);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mColorIndicator.setPageSlider(mPager);
        mColorIndicator.setItemOffset(0);
        mColorIndicator.setItemElement(
                ResUtil.getCustomRectGradientDrawable(Color.CYAN, new Rect(0, 0, Const.TWO40, Const.FIVE)),
                ResUtil.getCustomRectGradientDrawable(Color.CYAN, new Rect(0, 0, Const.TWO40, Const.TEN)));
        IntentParams intentParams = intent.getParams();
        if (intentParams != null) {
            if (intentParams.getParam(Const.KEY_VIA_LAYOUT) != null) {
                initPager(Color.BLACK);
                mDlViewRoot.setBackground(ResUtil.buildDrawableByColor(new Color(Const.RGB_COLOR).getValue()));
                mColorIndicator.setTabStyleViaLayout();
            }
        }
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int val, float floval, int value) {
                // Do something
            }

            @Override
            public void onPageSlideStateChanged(int val) {
            }

            @Override
            public void onPageChosen(int position) {
                mTabIndicator.selectTab(mTabIndicator.getTabAt(position));
            }
        });
    }

    private void initTabs() {
        mTabIndicator.setTabTextSize(Const.FIFTY);
        mTabIndicator.setTabLength(Const.TWO50);
        Element createBackground = ResUtil.buildDrawableByColor(Color.BLUE.getValue());
        mTabIndicator.setOrientation(Component.HORIZONTAL);
        mTabIndicator.setBackground(createBackground);
        mTabIndicator.setCentralScrollMode(true);
        mTabIndicator.setTabTextAlignment(TextAlignment.CENTER);
        mTabIndicator.setTabTextColors(Color.WHITE.getValue(), Color.GRAY.getValue());
        for (TabInfo next : mPageViews) {
            TabList tabList = mTabIndicator;
            Objects.requireNonNull(tabList);
            TabList.Tab tab = new TabList(this).new Tab(this);
            tab.setFocusable(Component.FOCUS_ENABLE);
            tab.setText(next.getName());
            tab.setAroundElementsPadding(Const.TWENTY);
            tab.setTextAlignment(TextAlignment.CENTER);
            tab.setBackground(ResUtil.buildDrawableByColor(Color.DKGRAY.getValue()));
            mTabIndicator.addTab(tab);
        }
        mTabIndicator.addTabSelectedListener(new TabListListener(this, mPageViews, mPager));
        mTabIndicator.setFixedMode(true);
        mTabIndicator.selectTab(mTabIndicator.getTabAt(0));
    }

    private void initPager(Color color) {
        mPageViews = new ArrayList();
        Intent intent1 = new Intent();
        intent1.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        mPageViews.add(new SamplePageView(this, pageText[Const.ZERO], color));
        mPageViews.add(new AccordionPageView(this, pageText[Const.ONE], color));
        mPageViews.add(new ListPageView(this, pageText[Const.TWO], color));
        mPageViews.add(new HorizontalPageView(this, pageText[Const.THREE], color));
        mPageViews.add(new SeekBarPageView(this, pageText[Const.FOUR], color));
        pageViewAdapter = new PageViewAdapter(this, mPageViews);
        mPager.setProvider(pageViewAdapter);
    }
}

