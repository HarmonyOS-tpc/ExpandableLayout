/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.Const;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;

/**
 * SamplePageView.
 */
public class HorizontalPageView extends AbstractPageView implements
        Component.ClickedListener, ExpandableLayout.OnExpansionUpdateListener {
    private ExpandableLayout expandableLayout;
    private Image expandButton;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public HorizontalPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public HorizontalPageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public HorizontalPageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_horizontal,
                        null, false);
        if (layout.findComponentById(ResourceTable.Id_expandable_layout) instanceof ExpandableLayout) {
            expandableLayout = (ExpandableLayout) layout.findComponentById(ResourceTable.Id_expandable_layout);
        }
        if (layout.findComponentById(ResourceTable.Id_expand_button) instanceof Image) {
            expandButton = (Image) layout.findComponentById(ResourceTable.Id_expand_button);
        }
        initValue();
        return layout;
    }

    private void initValue() {
        expandableLayout.setOnExpansionUpdateListener(this);
        expandButton.setClickedListener(this);
        expandableLayout.expand(false);
        expandableLayout.collapse(false);
    }

    @Override
    public void onExpansionUpdate(float expansionFraction, int state) {
        expandButton.setRotation(expansionFraction * Const.KEY_NO_180);
    }

    @Override
    public void onClick(Component component) {
        expandableLayout.toggle();
    }
}
