/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.cachapa.expandablelayout.demo.ResourceTable;
import net.cachapa.expandablelayout.demo.util.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;

/**
 * SamplePageView.
 */
public class AccordionPageView extends AbstractPageView implements Component.ClickedListener {
    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;

    /**
     * SamplePageView.
     *
     * @param abilitySlice AbilitySlice
     * @param name         String
     * @param txtColor     Color
     */
    public AccordionPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        super(abilitySlice, name, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param name           String
     * @param iconId         integer
     * @param iconIdSelected integer
     * @param txtColor       Color
     */
    public AccordionPageView(AbilitySlice abilitySlice, String name, int iconId, int iconIdSelected, Color txtColor) {
        super(abilitySlice, name, iconId, iconIdSelected, txtColor);
    }

    /**
     * SamplePageView.
     *
     * @param abilitySlice   AbilitySlice
     * @param txtColor       Color
     * @param redirectIntent AbilitySlice
     */
    public AccordionPageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice redirectIntent) {
        super(abilitySlice, txtColor, redirectIntent);
    }

    @Override
    public void initView() {
        super.setRootView(loadView());
    }

    /**
     * SamplePageView.
     *
     * @return layout Component
     */
    private Component loadView() {
        ComponentContainer layout = (ComponentContainer)
                LayoutScatter.getInstance(super.getSlice()).parse(ResourceTable.Layout_ability_accordion,
                        null, false);
        if (layout.findComponentById(ResourceTable.Id_expandable_layout_0) instanceof ExpandableLayout) {
            expandableLayout0 = (ExpandableLayout)
                    layout.findComponentById(ResourceTable.Id_expandable_layout_0);
        }
        if (layout.findComponentById(ResourceTable.Id_expandable_layout_1) instanceof ExpandableLayout) {
            expandableLayout1 = (ExpandableLayout)
                    layout.findComponentById(ResourceTable.Id_expandable_layout_1);
        }
        expandableLayout0.setOnExpansionUpdateListener((expansionFraction, state) ->
                LogUtil.debug("ExpandableLayout0", "State: " + state));

        expandableLayout1.setOnExpansionUpdateListener((expansionFraction, state) ->
                LogUtil.debug("ExpandableLayout3", "State: " + state));
        layout.findComponentById(ResourceTable.Id_expand_button_0).setClickedListener(this);
        layout.findComponentById(ResourceTable.Id_expand_button_1).setClickedListener(this);
        expandableLayout0.setExpanded(true, false);
        expandableLayout0.collapse(false);
        expandableLayout0.setDuration(1000);
        expandableLayout1.setDuration(1000);
        return layout;
    }


    @Override
    public void onClick(Component component) {
        if (component.getId() == (ResourceTable.Id_expand_button_0)) {
            expandableLayout0.expand();
            expandableLayout1.collapse();
        } else {
            expandableLayout0.collapse();
            expandableLayout1.expand();
        }
    }
}
