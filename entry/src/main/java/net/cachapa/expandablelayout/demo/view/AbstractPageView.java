/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view;

import net.cachapa.expandablelayout.demo.util.LogUtil;
import net.cachapa.expandablelayout.demo.view.tab.TabInfo;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

/**
 * AbstractPageView.
 */
public abstract class AbstractPageView implements PageInfo, TabInfo {
    private static final String TAG = AbstractPageView.class.getCanonicalName();
    private AbilitySlice slice;
    private String name;
    private int iconId = 0;
    private int iconIdSelected = 0;
    private Component rootView;
    private Color txtColor;

    /**
     * initView.
     */
    public abstract void initView();

    /**
     * AbstractPageView.
     *
     * @param abilitySlice AblitySlice
     * @param name String
     * @param txtColor Color
     */
    public AbstractPageView(AbilitySlice abilitySlice, String name, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.debug(TAG, "net.cachapa.expandablelayoutdemo.slice is null," +
                    " default net.cachapa.expandablelayoutdemo.slice");
            this.slice = new AbilitySlice();
        }
        this.name = name;
        this.txtColor = txtColor;
        initView();
    }

    /**
     * AbstractPageView.
     *
     * @param abilitySlice AblitySlice
     * @param str String
     * @param iconId integer
     * @param iconIdSelected integer
     * @param txtColor Color
     */
    public AbstractPageView(AbilitySlice abilitySlice, String str, int iconId, int iconIdSelected, Color txtColor) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.debug(TAG, "net.cachapa.expandablelayoutdemo.slice is null, " +
                    "set default net.cachapa.expandablelayoutdemo.slice");
            this.slice = new AbilitySlice();
        }
        this.name = str;
        this.iconId = iconId;
        this.iconIdSelected = iconIdSelected;
        this.txtColor = txtColor;
        initView();
    }

    /**
     * AbstractPageView.
     *
     * @param abilitySlice AblitySlice
     * @param txtColor Color
     * @param abilitySliceDetails AbilitySlice
     */
    public AbstractPageView(AbilitySlice abilitySlice, Color txtColor, AbilitySlice abilitySliceDetails) {
        if (abilitySlice != null) {
            this.slice = abilitySlice;
        } else {
            LogUtil.debug(TAG, "net.cachapa.expandablelayoutdemo.slice is null, " +
                    "set default net.cachapa.expandablelayoutdemo.slice");
            this.slice = new AbilitySlice();
        }
        this.txtColor = txtColor;
        this.slice = abilitySliceDetails;
    }

    /**
     * getSlice.
     *
     * @return ablitySlice
     */
    public AbilitySlice getSlice() {
        return this.slice;
    }

    /**
     * getName.
     *
     * @return string
     */
    public String getName() {
        return this.name;
    }

    /**
     * getIconId.
     *
     * @return integer
     */
    public int getIconId() {
        return this.iconId;
    }

    /**
     * getIconIdSelected.
     *
     * @return integer
     */
    public int getIconIdSelected() {
        return this.iconIdSelected;
    }

    /**
     * setIconIdSelected.
     *
     * @param val Value
     */
    public void setIconIdSelected(int val) {
        this.iconIdSelected = val;
    }

    /**
     * setIconIdSelected.
     *
     * @param val Value
     */
    public void setIconId(int val) {
        this.iconId = val;
    }

    /**
     * getTxtColor.
     *
     * @return val color
     */
    public Color getTxtColor() {
        return this.txtColor;
    }

    /**
     * setTxtColor.
     *
     * @param txtColor txtColor
     */
    public void setTxtColor(Color txtColor) {
        this.txtColor = txtColor;
    }

    /**
     * getRootView.
     *
     * @return Component
     */
    public Component getRootView() {
        return this.rootView;
    }

    /**
     * getRootView.
     *
     * @param component Component
     */
    public void setRootView(Component component) {
        this.rootView = component;
    }
}