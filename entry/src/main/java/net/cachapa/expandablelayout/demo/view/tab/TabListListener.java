/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.demo.view.tab;

import net.cachapa.expandablelayout.demo.util.LogUtil;
import net.cachapa.expandablelayout.demo.util.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;

import java.util.List;
import java.util.Optional;

/**
 * TabListListener.
 */
public class TabListListener implements TabList.TabSelectedListener {
    private static final String TAG = TabListListener.class.getCanonicalName();
    private PageSlider pager;
    private AbilitySlice slice;
    private List<? extends TabInfo> tabViews;

    /**
     * TabListListener.
     *
     * @param abilitySlice AblitySlice
     * @param list         tablist
     * @param pageSlider   Pageslider
     */
    public TabListListener(AbilitySlice abilitySlice, List<? extends TabInfo> list, PageSlider pageSlider) {
        this.slice = abilitySlice;
        this.tabViews = list;
        this.pager = pageSlider;
    }

    /**
     * onSelected.
     *
     * @param tab TabList
     */
    public void onSelected(TabList.Tab tab) {
        if (tab == null) {
            LogUtil.debug(TAG, "onTabSelected tab is null");
            return;
        }
        LogUtil.info(TAG, "Select tab " + tab.getPosition());
        PageSlider pageSlider = pager;
        if (pageSlider != null) {
            pageSlider.setCurrentPage(tab.getPosition(), true);
        }
        Optional<TabInfo> associateTabView = getAssociateTabView(tab);
        if (associateTabView.isPresent()) {
            if ((associateTabView.get().getIconIdSelected()) != 0) {
                ResUtil.createTabIcon(slice, tab, associateTabView.get().getIconIdSelected());
            }
        }
    }

    /**
     * onUnselected.
     *
     * @param tab TabList
     */
    public void onUnselected(TabList.Tab tab) {
        LogUtil.info(TAG, " onTabUnselected");
        if (tab == null) {
            LogUtil.debug(TAG, "onTabUnselected tab is null");
            return;
        }
        Optional<TabInfo> associateTabView = getAssociateTabView(tab);
        if (associateTabView.isPresent()) {
            if (associateTabView.get().getIconId() != 0) {
                ResUtil.createTabIcon(slice, tab, associateTabView.get().getIconId());
            }
        }
    }

    /**
     * getAssociateTabView.
     *
     * @param tab TabList
     * @return empty
     */
    private Optional<TabInfo> getAssociateTabView(TabList.Tab tab) {
        int position = tab.getPosition();
        if (position >= 0 && position <= tabViews.size()) {
            return Optional.ofNullable((TabInfo) tabViews.get(position));
        }
        return Optional.empty();
    }

    /**
     * onReselected.
     *
     * @param tab TabList
     */
    public void onReselected(TabList.Tab tab) {
        LogUtil.info(TAG, " onTabReselected");
    }
}