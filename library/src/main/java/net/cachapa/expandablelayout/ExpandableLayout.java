package net.cachapa.expandablelayout;

import net.cachapa.expandablelayout.util.Constants;
import net.cachapa.expandablelayout.util.FastOutSlowInInterpolator;
import net.cachapa.expandablelayout.util.LogUtil;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.global.configuration.Configuration;

import static net.cachapa.expandablelayout.ExpandableLayout.State.*;


/**
 * MainAbilitySlice.
 */
public class ExpandableLayout extends StackLayout implements Component.DrawTask {
    /**
     * KEY_SUPER_STATE
     */
    public static final String KEY_SUPER_STATE = "super_state";
    /**
     * KEY_EXPANSION
     */
    public static final String KEY_EXPANSION = "expansion";
    /**
     * HORIZONTAL
     */
    public static final int HORIZONTAL = 0;
    /**
     * VERTICAL
     */
    public static final int VERTICAL = 1;
    private static final String EL_DURATION = "el_duration";
    private static final String EL_EXPANDED = "el_expanded";
    private static final String EL_PARALLAX = "el_parallax";
    private static final String EL_ORENTATION = "ohos:orientation";
    private static final int DEFAULT_DURATION = 300;
    private int duration = DEFAULT_DURATION;
    private float parallax;
    private float expansion;
    private int orientation;
    private int state;

    private Animator.CurveType interpolator = new FastOutSlowInInterpolator();
    private AnimatorValue animator;

    private OnExpansionUpdateListener listener;

    /**
     * State interface.
     */
    public interface State {
        /**
         * COLLAPSED
         */
        int COLLAPSED = 0;
        /**
         * COLLAPSING
         */
        int COLLAPSING = 1;
        /**
         * EXPANDING
         */
        int EXPANDING = 2;
        /**
         * EXPANDED
         */
        int EXPANDED = 3;
    }

    /**
     * ExpandableLayout constructor
     */
    public ExpandableLayout() {
        super(null);
    }

    /**
     * ExpandableLayout constructor
     *
     * @param context Context
     */
    public ExpandableLayout(Context context) {
        this(context, null);
    }

    /**
     * ExpandableLayout constructor
     *
     * @param context Context
     * @param attrs   Attrset
     */
    public ExpandableLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        if (attrs != null) {
            duration = attrs.getAttr(EL_DURATION).isPresent()
                    ? attrs.getAttr(EL_DURATION).get().getIntegerValue() : DEFAULT_DURATION;
            expansion = attrs.getAttr(EL_EXPANDED).isPresent() ?
                    attrs.getAttr(EL_EXPANDED).get().getIntegerValue() : 1;
            parallax = attrs.getAttr(EL_PARALLAX).isPresent()
                    ? attrs.getAttr(EL_PARALLAX).get().getDimensionValue() : 1;
            orientation = attrs.getAttr(EL_ORENTATION).isPresent()
                    ? attrs.getAttr(EL_ORENTATION).get().getIntegerValue() : VERTICAL;
            state = expansion == 0 ? COLLAPSED : EXPANDED;
            setParallax(parallax);
            attrs.getStyle();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        onMeasure(getWidth(), getHeight());
    }

    /**
     * ExpandableLayout constructor
     *
     * @param widthMeasureSpec  width
     * @param heightMeasureSpec height
     */
    private void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int size = orientation == DirectionalLayout.HORIZONTAL ? widthMeasureSpec : heightMeasureSpec;

        setVisibility(expansion == 0 && size == 0 ? HIDE : VISIBLE);
        int expansionDelta = size - Math.round(size * expansion);
        if (parallax > 0) {
            float parallaxDelta = expansionDelta * parallax;
            for (int ival = 0; ival < getChildCount(); ival++) {
                Component child = getComponentAt(ival);
                if (orientation == HORIZONTAL) {
                    int direction = Constants.MINUS_ONE_NUM;
                    child.setTranslationX(direction * parallaxDelta);
                } else {
                    child.setTranslationY(-parallaxDelta);
                }
            }
        }

        if (orientation == HORIZONTAL) {
            setComponentSize(widthMeasureSpec - expansionDelta, heightMeasureSpec);
        } else {
            setComponentSize(widthMeasureSpec, heightMeasureSpec - expansionDelta);
        }
    }

    /**
     * onConfigurationChanged method
     *
     * @param newConfig Configuration
     */
    protected void onConfigurationChanged(Configuration newConfig) {
        if (animator != null) {
            animator.cancel();
        }
    }

    /**
     * Get expansion state
     *
     * @return one of {@link State}
     */
    public int getState() {
        return state;
    }

    /**
     * isExpanded
     *
     * @return true
     */
    public boolean isExpanded() {
        Boolean val = state == EXPANDING || state == EXPANDED;
        return val;
    }

    /**
     * toggle
     */
    public void toggle() {
        toggle(true);
    }

    /**
     * toggle method
     *
     * @param animate boolean
     */
    public void toggle(boolean animate) {
        if (isExpanded()) {
            collapse(animate);
        } else {
            expand(animate);
        }
    }

    /**
     * toggle method
     */
    public void expand() {
        expand(true);
    }

    /**
     * expand method
     *
     * @param animate boolean
     */
    public void expand(boolean animate) {
        setExpanded(true, animate);
    }

    /**
     * collapse method
     */
    public void collapse() {
        collapse(true);
    }

    /**
     * collapse method
     *
     * @param animate Animation
     */
    public void collapse(boolean animate) {
        setExpanded(false, animate);
    }

    /**
     * setExpanded method
     *
     * @param expand Animation
     */
    public void setExpanded(boolean expand) {
        setExpanded(expand, true);
    }

    /**
     * setExpanded method
     *
     * @param expand  boolean
     * @param animate boolean
     */
    public void setExpanded(boolean expand, boolean animate) {
        LogUtil.debug("ExpandableLayout0", "expande setExpanded" + expand + "==" + animate);
        if (expand == isExpanded()) {
            return;
        }

        int targetExpansion = expand ? 1 : 0;
        if (animate) {
            LogUtil.debug("ExpandableLayout0", "expande animate" + expand + "==" + targetExpansion);
            animateSize(targetExpansion);
        } else {
            LogUtil.debug("ExpandableLayout0", "expande else" + expand + "==" + targetExpansion);
            setExpansion(targetExpansion);
        }
    }

    public int getDuration() {
        return duration;
    }

    public void setInterpolator(Animator.CurveType interpolator) {
        this.interpolator = interpolator;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getExpansion() {
        return expansion;
    }

    /**
     * setExpanded method
     *
     * @param expansion float
     */
    public void setExpansion(float expansion) {
        if (this.expansion == expansion) {
            return;
        }

        // Infer state from previous value
        float delta = expansion - this.expansion;
        if (expansion == 0) {
            state = COLLAPSED;
        } else if (expansion == 1) {
            state = EXPANDED;
        } else if (delta < 0) {
            state = COLLAPSING;
        } else if (delta > 0) {
            state = EXPANDING;
        }

        setVisibility(state == COLLAPSED ? HIDE : VISIBLE);
        this.expansion = expansion;
        postLayout();

        if (listener != null) {
            listener.onExpansionUpdate(expansion, state);
        }
    }

    public float getParallax() {
        return parallax;
    }

    /**
     * setExpanded method
     *
     * @param parallax float
     */
    public void setParallax(float parallax) {
        parallax = Math.min(1, Math.max(0, parallax));
        this.parallax = parallax;
    }

    /**
     * setOrientation method
     *
     * @param orientation integer
     */
    public void setOrientation(int orientation) {
        if (orientation < 0 || orientation > 1) {
            throw new IllegalArgumentException("Orientation must be either 0 (horizontal) or 1 (vertical)");
        }
        this.orientation = orientation;
    }

    /**
     * setOnExpansionUpdateListener method
     *
     * @param updateListener OnExpansionUpdateListener
     */
    public void setOnExpansionUpdateListener(OnExpansionUpdateListener updateListener) {
        this.listener = updateListener;
    }

    private void animateSize(int targetExpansion) {
        if (animator != null) {
            animator.cancel();
            animator = null;
        }

        animator = ofFloat(expansion, targetExpansion);
        animator.setCurveType(AnimatorValue.CurveType.ACCELERATE_DECELERATE);
        animator.setDuration(duration);

        animator.setValueUpdateListener((animatorValue, value) -> {
            setExpansion((float) animatorValue.getCurveType());
        });
        animator.setStateChangedListener(new ExpansionListener(targetExpansion));
        animator.start();
    }

    /**
     * AnimatorValue ofFloat method
     *
     * @param values ofFloat
     * @return anim AnimationValue
     */
    public static AnimatorValue ofFloat(float... values) {
        AnimatorValue anim = new AnimatorValue();
        return anim;
    }

    /**
     * OnExpansionUpdateListener.
     */
    public interface OnExpansionUpdateListener {
        /**
         * Callback for expansion updates
         *
         * @param expansionFraction float
         * @param state             Integer
         */
        void onExpansionUpdate(float expansionFraction, int state);
    }

    /**
     * ExpansionListener.
     */
    private class ExpansionListener implements Animator.StateChangedListener {
        private int targetExpansion;
        private boolean isCanceled;

        /**
         * ExpansionListener ofFloat method
         *
         * @param targetExpansion integer
         */
        protected ExpansionListener(int targetExpansion) {
            this.targetExpansion = targetExpansion;
        }

        @Override
        public void onStart(Animator animator1) {
            state = targetExpansion == 0 ? COLLAPSING : EXPANDING;
        }

        @Override
        public void onStop(Animator animator1) {
            // Do something
        }

        @Override
        public void onCancel(Animator animator1) {
            isCanceled = true;
        }

        @Override
        public void onEnd(Animator animator1) {
            if (!isCanceled) {
                state = targetExpansion == 0 ? COLLAPSED : EXPANDED;
                setExpansion(targetExpansion);
            }
        }

        @Override
        public void onPause(Animator animator1) {
            // Do Something
        }

        @Override
        public void onResume(Animator animator1) {
            // Do do something
        }
    }
}
