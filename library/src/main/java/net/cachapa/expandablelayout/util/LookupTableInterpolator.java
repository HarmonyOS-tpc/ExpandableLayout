/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cachapa.expandablelayout.util;

import ohos.agp.animation.Animator;

/**
 * LookupTableInterpolator.
 */
public abstract class LookupTableInterpolator extends Animator.CurveType {
    private final float[] mValues;
    private final float mStepSize;

    /**
     * LookupTableInterpolator
     *
     * @param values float
     */
    protected LookupTableInterpolator(float[] values) {
        mValues = values;
        mStepSize = 1f / (mValues.length - 1);
    }

    /**
     * getInterpolation in-built animation interpolation method available
     *
     * @param input float
     * @return 0f float value
     */
    public float getInterpolation(float input) {
        if (input >= 1.0f) {
            return 1.0f;
        }
        if (input <= 0f) {
            return 0f;
        }
        int position = Math.min((int) (input * (mValues.length - 1)), mValues.length - Constants.TWO_NUM);
        float quantized = position * mStepSize;
        float diff = input - quantized;
        float weight = diff / mStepSize;
        return mValues[position] + weight * (mValues[position + 1] - mValues[position]);
    }
}
